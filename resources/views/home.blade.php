<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>hello</title>
    {{-- <script src="path/to/chartjs/dist/chart.umd.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        th,
        td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>

<body>
    <form action="{{ route('personaliserz') }}" method="post">
        @csrf
        <select name="axe_x_choice" id="">
            @foreach ($headersCharts as $title)
                <option value="{{ $title }}">{{ $title }}</option>
            @endforeach
        </select>
        <select name="axe_y_choice" id="">
            @foreach ($headersCharts as $title)
                <option value="{{ $title }}">{{ $title }}</option>
            @endforeach
        </select>

        <button> valider</button>
    </form>


    <div style="width:80%; margin:auto;">
        <canvas id="populationChart" width="400" height="200"></canvas>
    </div>

    <table>
        <thead>
            <tr>
                @foreach ($headers as $header)
                    <th> {{ $header }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($list as $oneList)
                <tr>
                    @foreach ($oneList as $key => $value)
                        <td>{{ $key }}</td>
                    @endforeach
                </tr>
            @endforeach


        </tbody>
    </table>

</body>

<script>
    // Extract region names and population values from the associative array
    var regions = <?php echo json_encode(array_column($results, $inputChoiceX)); ?>;
    var populations = <?php echo json_encode(array_column($results, $inputChoiceY)); ?>;

    // Create a bar chart
    var ctx = document.getElementById('populationChart').getContext('2d');
    var choice = ['bar', 'line'];
    var randomIndex = Math.floor(Math.random() * choice.length);
    var randomChartType = choice[randomIndex];
    var populationChart = new Chart(ctx, {
        type: randomChartType,
        data: {
            labels: regions,
            datasets: [{
                label: 'Population',
                data: populations,
                backgroundColor: 'blue',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: false
                },
                x: {
                    beginAtZero: false

                }
            }
        }
    });
</script>



</html>
