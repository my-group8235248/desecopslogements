# Utilisez une image officielle PHP avec Apache
FROM php:8.2-fpm-alpine


# Installez les dépendances nécessaires pour Laravel
RUN apk --no-cache add libzip-dev unzip \
    && docker-php-ext-install zip pdo_mysql
# RUN apt-get update && \
#     apt-get install -y \
#         libzip-dev \
#         unzip \
#         && docker-php-ext-install zip pdo_mysql

# Copiez les fichiers de l'application Laravel dans le conteneur
COPY . /var/www/html


# Définissez les autorisations appropriées
RUN chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache

# Installez Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# Installez les dépendances de l'application Laravel
RUN composer install --no-interaction --optimize-autoloader



# Exposez le port Apache
EXPOSE 80

# Commande par défaut pour démarrer Apache
CMD ["apache2-foreground"]
