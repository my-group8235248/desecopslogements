<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Statement;

class HomeController extends Controller
{
    public function __invoke(Request $request)
    {
       

        $csvFile = public_path('storage/logements-sociaux.csv');
        if (!file_exists($csvFile)) {
            return redirect()->back()->with('error', 'aucune data');
        }
        $csv = Reader::createFromPath($csvFile);

        $stmt = (new Statement())
            ->offset(1)
            ->limit(100);

        // Fetch the header from the CSV file
        $headers = $stmt->process($csv)->fetchOne();

        // Create a statement to fetch records with named columns (skip the header row)
        $dataStatement = (new Statement())->offset(12);

        // Fetch the CSV records with named columns
        $records = $dataStatement->process($csv);

        $list = [];
        $Listheaders = 0;
        $records = $csv->getRecords();

        foreach ($records as $record) {
            $allHeaders =  explode(';', $record[0]);
            foreach ($headers as $index => $header) {
                $values = explode(';', $header);
                $i = 1;
                if (count($values) < count($allHeaders)) {
                    $values["no_name" . $i++] = 'no value';
                }
                if (count($values) > count($allHeaders)) {
                    dd($values, $allHeaders);
                    // $values["no_name" . $i++] = 'no value';
                }
                $combinedArray = array_combine($allHeaders, $values);

                $list[] = $combinedArray;
            }
        }
        $headers = array_keys($list[0]);

        $headersCharts = array_slice($headers, 0, 20, true);
        $headers = array_slice($headers, 0, 5, true);
        $theGoodValuesList = [];
        foreach ($list as $one => $two) {


            if ($one > 15) {
                // dd($two);
                $two = array_slice($two, 0, 5, true);
                $theGoodValuesList[] = $two;
            }
            if ($one > 100) {
                break;
            }
        }


        $reader = Reader::createFromPath($csvFile, 'r');
        $reader->setHeaderOffset(0);
        $results = [];
        foreach ($reader->getRecords() as $records) {
            $results[] = $records;
        }


        $outputArray = [];
        $i = 0;
        foreach ($results as $result) {
            foreach ($result as $key => $value) {
                $explodedValues = explode(';', $value);
                $outputArray[] = array_combine(
                    explode(';', $key),
                    $explodedValues
                );
            }
            if ($i > 30) {
                break;
            }
            $i++;
            # code...
        }

        $inputChoiceX = 'annee_publication';
        $inputChoiceY = 'nombre_d_habitants';
        $lesResultats = $outputArray;
        if ($request->post()) {
            $inputChoiceX = $request->axe_x_choice;
            $inputChoiceY = $request->axe_y_choice;
        }


        return view('home', [
            'headersCharts' => $headersCharts,
            'inputChoiceX' => $inputChoiceX,
            'inputChoiceY' => $inputChoiceY,
            'list' => $theGoodValuesList, 'headers' => $headers, 'results' => $lesResultats
        ]);
    }
}
